/**
 *  package import
 */
import Vue from 'vue'
import App from './App.vue'
import Vuex from 'vuex'
import NProgress from 'nprogress'
import SweetAlert from 'vue-sweetalert2'
import Loading from 'vue-loading-overlay'
import Router from 'vue-router'
import axios from 'axios'
import VueCookies from 'vue-cookies'
import Vuelidate from 'vuelidate'
import 'vue-loading-overlay/dist/vue-loading.css';

/**
 * Components import
 */
import Login from '@/components/Auth/Login'
import Register from '@/components/Auth/Register'
import Dashboard from '@/components/Dashboard/Dashboard'
import Dispenser from '@/components/Dashboard/Dispenser'
import Tank from '@/components/Dashboard/Tank'
import Report from '@/components/Dashboard/Report'


Vue.use(Vuex)
Vue.use(NProgress)
Vue.use(Vuelidate)
Vue.use(SweetAlert)
Vue.use(Loading)
Vue.use(VueCookies)
Vue.use(Router)
Vue.config.productionTip = false

// NProgress.configure({showSpinner:false });

Vue.prototype.$http = axios
Window.axios = axios
Vue.prototype.$cookies = VueCookies
// set default config
VueCookies.config('1m')
// set global cookie
VueCookies.set('theme', 'default');
VueCookies.set('hover-time', '1s');

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/login',
      component: Login,
      name: 'Login'
    },
    {
      path: '/register',
      component: Register,
      name: 'Register'
    },
    {
      path: '/dashboard',
      component: Dashboard,
      name: 'Dashboard',
      meta : {
        requiresAuth : true
      }
    },
    {
      path: '/tanks',
      component: Tank,
      name: 'Tank',
      meta : {
        requiresAuth : true
      }
    },
    {
      path: '/dispensers',
      component: Dispenser,
      name: 'Dispenser'
    },
    {
      path: '/reports',
      component: Report,
      name: 'Report',
      meta : {
        requiresAuth : true
      }
    },
  ]
})

const store = new Vuex.Store({
  state: {
    // base_url: 'http://localhost:8000/api/',
    base_url: 'http://inv-app1.herokuapp.com/api/',
    status: '',
    token: localStorage.getItem('token') || '',
    // token: $cookies.get('token'),
    user: $cookies.get('user')
  },
  mutations: {
    auth_request(state) {
      state.status = 'loading'
    },
    auth_success(state, token, user) {
      state.status = 'success'
      state.token = token
      state.user = user
    },
    auth_error(state) {
      state.status = 'error'
    },
    logout(state) {
      state.status = ''
      state.token = ''
    },
    userDetails(state, user) {
      state.user = user
    },
    profileUpdate(state, user) {
      state.user = user

    }
  },
  actions: {
    login({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request')
        let url = store.state.base_url + 'auth/authenticate'
        axios({ url: url, data: user, method: 'POST' })
          .then(resp => {
            const token = resp.data.token
            const user = resp.data.user
            // $cookies.set('token',token)
            $cookies.set('user', user)
            localStorage.setItem('token', token)
            // axios.defaults.headers.common['Authorization'] = token
            commit('auth_success', token, user)
            resolve(resp)
          })
          .catch(err => {
            commit('auth_error')
            // $cookies.remove('token')
            $cookies.remove('user')
            localStorage.removeItem('token')
            reject(err)
          })
      })
    },

    register({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('auth_request')
        let url = store.state.base_url + 'auth/register'
        axios({ url: url, data: user, method: 'POST' })
          .then(resp => {
            const token = resp.data.token
            const user = resp.data.user
            // $cookies.set('token',token)
            $cookies.set('user', user)
            localStorage.setItem('token', token)
            // axios.defaults.headers.common['Authorization'] = token
            commit('auth_success', token, user)
            resolve(resp)
          })
          .catch(err => {
            commit('auth_error', err)
            // $cookies.remove('token')
            $cookies.remove('user')
            localStorage.removeItem('token')
            reject(err)
          })
      })
    },

    logout({ commit }) {
      return new Promise((resolve, reject) => {
        commit('logout')
        localStorage.removeItem('token')
        $cookies.remove('user')
        // delete axios.defaults.headers.common['Authorization']
        resolve()
      })
    },
    profileUpdate({ commit }, user) {
      return new Promise((resolve, reject) => {
        commit('profileUpdate', user)
        $cookies.set('user', user)

        resolve()
      })
    },
  },

  getters: {
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    user: state => state.user
  }
})

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (store.getters.isLoggedIn) {
      next()
      return
    }
    next('/login')
  } else {
    next()
  }
})

router.beforeEach((to, from, next) => {
  NProgress.start()
  next()
})
router.afterEach(() => {
  NProgress.done(true)
})

new Vue({
  render: h => h(App),
  router,
  store,
}).$mount('#app')
